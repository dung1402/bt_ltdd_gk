package com.example.b5;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import com.example.b5.databinding.ActivityListViewBinding;
import java.util.ArrayList;
public class ListView extends AppCompatActivity {
ActivityListViewBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityListViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        int[] imageId = {R.drawable.anh, R.drawable.pro1, R.drawable.pro2, R.drawable.pro8, R.drawable.pro4,
                R.drawable.pro5, R.drawable.pro6, R.drawable.pro7, R.drawable.pro8};
        String[] name = {"Thuỳ Dung", "Thiên Phú", "Ngọc Tuấn", "Mạnh Quyết", "lan anh", "linh đan", "Toàn", "Ivana", "Alex"};
        String[] fullname = {"Dương Thị Thuỳ Dung", "Huỳnh Thiên Phú", "Trần Ngọc Tuấn", "Trần Mạnh Quyết", "lan anh", "linh đan", "Toàn", "Ivana", "Alex"};
        String[] MSV={"1811505310206","1811505120140","1811505120124","1811503120140","18224030202","18114042010","183940432","18181020334","3810101011"};
        String[] lastMessage = {"Hello", "Supp", "Let's Catchup", "Dinner tonight?", "Gotta go",
                "i'm in meeting", "Gotcha", "Let's Go", "any Weekend Plans?"};
        String[] lastmsgTime = {"8:45 pm", "9:00 am", "7:34 pm", "6:32 am", "5:76 am",
                "5:00 am", "7:34 pm", "2:32 am", "7:76 am"};
        String[] phoneNo = {"0766723330", "0777557671", "0961116407", "0961462937", "5434432343",
                "9439043232", "7534354323", "6545543211", "7654432343"};
        String[] country = {"hà tĩnh", "Daklak", "Hà tĩnh", "hà Tĩnh", "hà nội", "HCM", "hà nam", "France", "Switzerland"};

        ArrayList<User> userArrayList = new ArrayList<>();
        for (int i = 0; i < imageId.length; i++) {
            User user = new User(name[i],fullname[i],MSV[i] ,lastMessage[i], lastmsgTime[i], phoneNo[i], country[i], imageId[i]);
            userArrayList.add(user);
        }
        ListAdapter listAdapter = new ListAdapter(ListView.this, userArrayList);

        binding.listview.setAdapter(listAdapter);
        binding.listview.setClickable(true);
        binding.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ListView.this, UserActivity.class);
                i.putExtra("name", fullname[position]);
                i.putExtra("msv",MSV[position]);
                i.putExtra("phone", phoneNo[position]);
                i.putExtra("country", country[position]);
                i.putExtra("imageid", imageId[position]);
                startActivity(i);
            }
            });
        }
}
