package com.example.b5;

public class User {

    String name, fullname,MSV,lastMessage, lastMsgTime, phoneNo, country;
    int imageId;

    public User(String name,String fullname,String MSV, String lastMessage, String lastMsgTime, String phoneNo, String country, int imageId) {
        this.name = name;
        this.fullname = fullname;
        this.MSV =MSV;
        this.lastMessage = lastMessage;
        this.lastMsgTime = lastMsgTime;
        this.phoneNo = phoneNo;
        this.country = country;
        this.imageId = imageId;
    }
}
