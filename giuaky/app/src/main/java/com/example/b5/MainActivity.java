package com.example.b5;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button bt_login;
    TextView txt_signup;
    EditText email,password;
    CheckBox chbRemember;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt_login=(Button)findViewById(R.id.buttonLogin);
        txt_signup=(TextView)findViewById(R.id.txtSignUp);
        email=(EditText)findViewById(R.id.inputEmail);
        password=(EditText)findViewById(R.id.inputPassword) ;
        chbRemember = findViewById(R.id.chbRemember);
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=getIntent();
                String dl_mail=i.getStringExtra("mail").trim();
                String dl_pass=i.getStringExtra("pass").trim();
                String txt_mail = email.getText().toString().trim();
                String txt_pass=password.getText().toString().trim();
                if(txt_mail.isEmpty() || txt_pass.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Không được để trống",Toast.LENGTH_SHORT).show();
                }
                else {
                    if((txt_pass.equals(dl_pass)) && (txt_mail.equals(dl_mail))){
                        Toast.makeText(getApplicationContext(),"Đăng nhập thành công",Toast.LENGTH_SHORT).show();
                        Intent listItem=new Intent( MainActivity.this, ListView.class);
                        startActivity(listItem);
                    }
                    else
                        Toast.makeText(getApplicationContext(),"Mật khẩu hoặc pass sai",Toast.LENGTH_SHORT).show();
                }
            }
        });
        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent( MainActivity.this, Singup.class);
                startActivity(i);
            }
        });
        chbRemember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean Checked) {
                String message=Checked ? "tài khoản của bạn sẽ được ghi nhớ" :"bạn cần  nhâp";
                Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
            }
        });
    }
}